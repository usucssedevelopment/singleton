﻿namespace Singleton
{
    /// <summary>
    /// BaseSingleton
    /// 
    /// Encapsulates a list of integers
    /// </summary>
    public class BaseSingleton<T> where T : new()
    {
        private static BaseSingleton<T> _internalInstance;
        private static readonly object MyLock = new Lock<T>();

        public static BaseSingleton<T> Instance
        {
            get
            {
                lock (MyLock)
                {
                    if (_internalInstance == null)
                        _internalInstance = new BaseSingleton<T>();
                }
                return _internalInstance;
            }
        }

        public T Value { get; } = new T();

        // ReSharper disable once UnusedTypeParameter
        public class Lock<TLock> { }
    }
}
