﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Singleton;

namespace SingletonTest
{
    [TestClass]
    public class BaseSingletonTest
    {
        [TestMethod]
        public void BaseSingleton_TestEverything()
        {
            var singleton = BaseSingleton<List<int>>.Instance;
            Assert.IsNotNull(singleton);
            Assert.AreEqual(typeof(BaseSingleton<List<int>>), singleton.GetType());
            Assert.IsNotNull(singleton.Value);

            var singleton2 = BaseSingleton<List<int>>.Instance;
            Assert.AreSame(singleton, singleton2);

            var singleton3 = BaseSingleton<List<string>>.Instance;
            Assert.AreNotSame(singleton, singleton3);
            Assert.IsNotNull(singleton3.Value);

        }


    }
}
