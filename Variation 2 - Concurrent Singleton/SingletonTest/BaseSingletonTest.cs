﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Singleton;

namespace SingletonTest
{
    [TestClass]
    public class BaseSingletonTest
    {
        [TestMethod]
        public void BaseSingleton_TestInstance()
        {
            var singleton = BaseSingleton.Instance;
            Assert.IsNotNull(singleton);
            Assert.AreEqual(typeof(BaseSingleton), singleton.GetType());

            var singleton2 = BaseSingleton.Instance;
            Assert.AreSame(singleton, singleton2);

            var singleton3 = BaseSingleton.Instance;
            Assert.AreSame(singleton, singleton3);
        }

        [TestMethod]
        public void BaseSingleton_TestAddRemoveGet()
        {
            BaseSingleton.Instance.Add(10);
            Assert.AreEqual(1, BaseSingleton.Instance.Integers.Length);
            Assert.AreEqual(10, BaseSingleton.Instance.Integers[0]);

            BaseSingleton.Instance.Add(13);
            Assert.AreEqual(2, BaseSingleton.Instance.Integers.Length);
            Assert.AreEqual(10, BaseSingleton.Instance.Integers[0]);
            Assert.AreEqual(13, BaseSingleton.Instance.Integers[1]);

            BaseSingleton.Instance.Add(52);
            Assert.AreEqual(3, BaseSingleton.Instance.Integers.Length);
            Assert.AreEqual(10, BaseSingleton.Instance.Integers[0]);
            Assert.AreEqual(13, BaseSingleton.Instance.Integers[1]);
            Assert.AreEqual(52, BaseSingleton.Instance.Integers[2]);

            BaseSingleton.Instance.Remove(13);
            Assert.AreEqual(2, BaseSingleton.Instance.Integers.Length);
            Assert.AreEqual(10, BaseSingleton.Instance.Integers[0]);
            Assert.AreEqual(52, BaseSingleton.Instance.Integers[1]);

        }

    }
}
