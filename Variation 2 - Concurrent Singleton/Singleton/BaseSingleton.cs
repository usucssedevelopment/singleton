﻿using System.Collections.Generic;

namespace Singleton
{
    /// <summary>
    /// BaseSingleton
    /// 
    /// Encapsulates a list of integers
    /// </summary>
    public class BaseSingleton
    {
        private static BaseSingleton _internalInstance;
        private static readonly object MyLock = new object();

        public static BaseSingleton Instance
        {
            get
            {
                lock (MyLock)
                {
                    if (_internalInstance == null)
                        _internalInstance = new BaseSingleton();
                }
                return _internalInstance;
            }
        }


        private readonly List<int> _integers = new List<int>();

        public int[] Integers => _integers.ToArray();

        public void Add(int x)
        {
            // More interesting stuff done here
            _integers.Add(x);
        }

        public void Remove(int x)
        {
            // More interesting stuff done here
            _integers.Remove(x);
        }

        public void ReverseIntList()
        {
            // Interesting stuff done here, e.g.,            
            _integers.Reverse();
        }
    }
}
