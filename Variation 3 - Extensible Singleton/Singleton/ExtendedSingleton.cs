﻿using System.Collections.Generic;

namespace Singleton
{
    /// <summary>
    /// ExtendedSingleton
    /// 
    /// Encapsulates a list of strings in addition to the list of integers that it inherits from BaseSingleton.
    /// </summary>
    public class ExtendedSingleton : BaseSingleton
    {
        public new static ExtendedSingleton Instance => GetInstance(typeof (ExtendedSingleton)) as ExtendedSingleton;

        private readonly List<string> _strings = new List<string>();

        public string[] Strings => _strings.ToArray();

        public void Add(string s)
        {
            _strings.Add(s);
        }

        public void Remove(string s)
        {
            _strings.Remove(s);
        }
    }
}
