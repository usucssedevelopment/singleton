﻿using System;
using System.Collections.Generic;

namespace Singleton
{
    /// <summary>
    /// BaseSingleton
    /// 
    /// Encapsulates a list of integers
    /// </summary>
    public abstract class BaseSingleton
    {
        protected static BaseSingleton InternalInstance;
        protected static readonly object MyLock = new object();

        /// <summary>
        /// This method is the heart of the Singleton.  It encapsulates the logic that guarentees only one instance of the class
        /// </summary>
        /// <param name="specialization"></param>
        /// <returns></returns>
        protected static BaseSingleton GetInstance(Type specialization)
        {
            lock (MyLock)
            {
                if (InternalInstance == null)
                    InternalInstance = Activator.CreateInstance(specialization) as BaseSingleton;
            }
            return InternalInstance;
        }
        public static BaseSingleton Instance => GetInstance(typeof(BaseSingleton));


        private readonly List<int> _integers = new List<int>();

        public int[] Integers => _integers.ToArray();

        public void Add(int x)
        {
            // More interesting stuff done here
            _integers.Add(x);
        }

        public void Remove(int x)
        {
            // More interesting stuff done here
            _integers.Remove(x);
        }

        public void ReverseIntList()
        {
            // Interesting stuff done here, e.g.,            
            _integers.Reverse();
        }
    }
}
