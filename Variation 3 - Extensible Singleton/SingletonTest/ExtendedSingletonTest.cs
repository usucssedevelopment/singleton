﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Singleton;

namespace SingletonTest
{
    [TestClass]
    public class ExtendedSingletonTest
    {
        [TestMethod]
        public void ExtendedSingleton_TestInstance()
        {
            var singleton = ExtendedSingleton.Instance;
            Assert.IsNotNull(singleton);
            Assert.AreEqual(typeof(ExtendedSingleton), singleton.GetType());

            var singleton2 = ExtendedSingleton.Instance;
            Assert.AreSame(singleton, singleton2);

            var singleton3 = BaseSingleton.Instance;
            Assert.AreSame(singleton, singleton3);
        }

        [TestMethod]
        public void ExtendedSingleton_TestAddRemoveGet()
        {
            ExtendedSingleton.Instance.Add(10);
            Assert.AreEqual(1, ExtendedSingleton.Instance.Integers.Length);
            Assert.AreEqual(10, ExtendedSingleton.Instance.Integers[0]);

            ExtendedSingleton.Instance.Add(13);
            Assert.AreEqual(2, ExtendedSingleton.Instance.Integers.Length);
            Assert.AreEqual(10, ExtendedSingleton.Instance.Integers[0]);
            Assert.AreEqual(13, ExtendedSingleton.Instance.Integers[1]);

            ExtendedSingleton.Instance.Add(52);
            Assert.AreEqual(3, ExtendedSingleton.Instance.Integers.Length);
            Assert.AreEqual(10, ExtendedSingleton.Instance.Integers[0]);
            Assert.AreEqual(13, ExtendedSingleton.Instance.Integers[1]);
            Assert.AreEqual(52, ExtendedSingleton.Instance.Integers[2]);

            ExtendedSingleton.Instance.Remove(13);
            Assert.AreEqual(2, ExtendedSingleton.Instance.Integers.Length);
            Assert.AreEqual(10, ExtendedSingleton.Instance.Integers[0]);
            Assert.AreEqual(52, ExtendedSingleton.Instance.Integers[1]);


            ExtendedSingleton.Instance.Add("Hello");
            Assert.AreEqual(1, ExtendedSingleton.Instance.Strings.Length);
            Assert.AreEqual("Hello", ExtendedSingleton.Instance.Strings[0]);

            ExtendedSingleton.Instance.Add("Testing");
            Assert.AreEqual(2, ExtendedSingleton.Instance.Strings.Length);
            Assert.AreEqual("Hello", ExtendedSingleton.Instance.Strings[0]);
            Assert.AreEqual("Testing", ExtendedSingleton.Instance.Strings[1]);

            ExtendedSingleton.Instance.Add("Bye");
            Assert.AreEqual(3, ExtendedSingleton.Instance.Strings.Length);
            Assert.AreEqual("Hello", ExtendedSingleton.Instance.Strings[0]);
            Assert.AreEqual("Testing", ExtendedSingleton.Instance.Strings[1]);
            Assert.AreEqual("Bye", ExtendedSingleton.Instance.Strings[2]);

            ExtendedSingleton.Instance.Remove("Testing");
            Assert.AreEqual(2, ExtendedSingleton.Instance.Strings.Length);
            Assert.AreEqual("Hello", ExtendedSingleton.Instance.Strings[0]);
            Assert.AreEqual("Bye", ExtendedSingleton.Instance.Strings[1]);

        }

    }
}
