# contacts

This repository contains variations of the singleton pattern.

* Variation 1 - Simple Singleton
  * A singleton that encapsulates a list of integers
* Variation 2 - Concurrent
  * A thread-safe singleton that encapsulates a list of integers
* Variation 3 - Extensible Singleton
  * A C# idiom for making a singleton that can be extended
  * This variation is problematic
    *  Since there is still only one instance of BaseSingleton, there can be at most one instance of one specialization
	*  Reuse via inheritence is not effective for singleton, in most class-based languages
* Variation 4 - Generic Singleton
  * A generic singleton that encapsulates any object of type T